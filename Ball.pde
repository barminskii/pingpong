class Ball {
  int size;
  float x;
  float y;
  float xv;
  float yv;
  float ax;
  float ay;
  float time;
  int points;
  
  
  int boardNumber;
  
  float a;
  float b;
  float c;
  float d;
  
  
  Ball() {
    x = 475;
    y = 360;
    xv = random(4, 4);
    yv = random(4, 4);
    ax = 2;
    ay = 2;
    size = 50;
    
  }
  
  
  void update() {
    x += xv;
    y += yv;
    handleBoxCollision();
  }
  
  void handleBoxCollision() {
    //println("==========");
    //println("Board #" + boardNumber);
    //println("x1: " + a + " y1: " + b + " || x2: " + c + " y2: " + d);
    //println("Ball");
    //println("x: " + x + " y: " + y);
    
    if ((x < 10) || (x > 940)) {
      if ((x < 10) && (boardNumber == 1)) {
        points++;
        x = 475;
        y = 360;
        xv = random(4, 5);
      } /*else if ((x > 940) && (boardNumber == 2)) {
        points++;
        x = 475;
        y = 360;
        xv = random(4, 5);
      }*/
    }
    if ((y < 10) || (y > 710)) {
      yv *= -1;
    }
  }
    
    
  void boardCollision() {
    if (((x < a+10) && (y > b) && (y < d)) && (boardNumber == 1)) {
      xv *= -1;
      if (abs(xv) <= 15) xv *= 1.05;
    }
    if (((x > a) && (y > b) && (y < d)) && (boardNumber == 2)) {
      xv *= -1;
      if (abs(xv) <= 15) xv *= 1.05;
    }
 }
 
  void getBoardVertex(float a, float b, float c, float d) {
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
    
  }
  
  void getBoardNumber(int boardNumber) {
    this.boardNumber = boardNumber;
  }
  
  void colorCondition() {
    //if (abs(xv) <= 5) fill(#FFFFFF);
    //if (abs(xv) > 5) fill(#F4DDF3);
    //if (abs(xv) > 6) fill(#F4D4E9);
    //if (abs(xv) > 7) fill(#F4D2EB);
    //if (abs(xv) > 8) fill(#F4C1DC);
    //if (abs(xv) > 9) fill(#F5AECC);
    //if (abs(xv) > 10) fill(#F49AB4);
    //if (abs(xv) > 11) fill(#F57FA0);
    //if (abs(xv) > 12) fill(#F5586E);
    //if (abs(xv) > 13) fill(#F4435B);
    //if (abs(xv) > 14) fill(#F5283F);
    //if (abs(xv) > 15) fill(#F5011A);
    fill(255, 159, 0, 200);
  }
  
  void display() {
    colorCondition();
    circle(x, y, size);
  }
  

  
}
