import ch.bildspur.postfx.builder.*;
import ch.bildspur.postfx.pass.*;
import ch.bildspur.postfx.*;

PostFX fx;

Ball ball;
Board player1 = new Board(50, 200, 1);
Board player2 = new Board(880, 200, 2);
Points points;
Points points2;


void setup() {
  size(950, 720, P3D);
  frameRate(60);
  fx = new PostFX(this);
  ball = new Ball();
  points = new Points(1, 0);
  points2 = new Points(2, 0);
  
}

void draw() {
  //background(#C6C6C6);
  background(0);
  
  // Разделение на поле
  fill(255, 159, 0, 200);
  rect(width/2, 0, 10, height);
  
  //Борд1
  ball.getBoardNumber(1);
  ball.getBoardVertex(player1.xVertex1, player1.yVertex1, player1.xVertex2, player1.yVertex2);
  player1.update();
  player1.display();
  
  ball.display();
  ball.update();
  ball.boardCollision();
  points.updatePoints(0);

  //Борд2
  ball.getBoardVertex(player2.xVertex1, player2.yVertex1, player2.xVertex2, player2.yVertex2);
  ball.getBoardNumber(2);
  player2.aiUpdate();
  player2.display();
  player2.getBallCoordinate(ball.x, ball.y);
  ball.getBoardNumber(2);
  
  ball.display();
  ball.update();
  ball.boardCollision();
  


  //Обновление очков 


  points.drawPlayerPoints();
  
  points2.updatePoints(ball.points);
  points2.drawPlayerPoints();
  
  //Постэфекты
  fx.render()
      .bloom(0.01, 25, 40.0)
      .noise(0.5, 8)
      .compose();
  
}
